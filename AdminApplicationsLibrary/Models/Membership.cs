﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[start_date] [datetime] NULL CONSTRAINT [DF__membershi__start__55009F39]  DEFAULT (NULL),
    //[end_date] [datetime] NULL CONSTRAINT [DF__membershi__end_d__55F4C372]  DEFAULT (NULL),
    //[membership_status_id] [int] NOT NULL,
    //[program_id] [int] NOT NULL,
    //[member_id] [int] NULL CONSTRAINT [DF__membershi__membe__56E8E7AB]  DEFAULT (NULL),
    //[is_program_leader] [bit] NOT NULL CONSTRAINT [DF__membershi__is_pr__57DD0BE4]  DEFAULT ((0)),
    //[is_facility_director] [bit] NOT NULL CONSTRAINT [DF__membershi__is_fa__58D1301D]  DEFAULT ((0)),
    //[is_cac_member] [bit] NOT NULL CONSTRAINT [DF__membershi__is_ca__59C55456]  DEFAULT ((0)),
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__membershi__lock___5CA1C101]  DEFAULT ((0)),
    //[original_end_date_before_termination] [datetime] NULL,


    [Table("memberships")]
    [DisplayColumn("Id", "start_date", true)]
    public class Membership
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }
 
        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Start Date")]
        public DateTime? start_date { get; set; }
        
        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "End date")]
        public DateTime? end_date { get; set; }
        
        [Display(Name = "Membership Status")]
        public int membership_status_id { get; set; }
        
        [Display(Name = "Program")]
        public int program_id { get; set; }

        [Display(Name = "Member Id")]
        public int? member_id { get; set; }
        
        [Display(Name = "Program Leader")]
        public Boolean is_program_leader { get; set; }
        
        [Display(Name = "Facility Director")]
        public Boolean is_facility_director { get; set; }
        
        [Display(Name = "Cac Member")]
        public Boolean is_cac_member { get; set; }

        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime updated_at { get; set; }

        [DataType(DataType.Date)] // added by TRD 10/5/2015
        //[DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Original End Date")]
        public DateTime? original_end_date_before_termination { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

        // The following fields are for display purposes only
        // they are not filled by the database

        public String Program;

        public String MembershipStatus;

    }
}