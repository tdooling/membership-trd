﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[medium_type] [varchar](50) NULL,


    [Table("member_promotion_mediums")]
    [DisplayColumn("Id", "medium_type")]
    public class Member_Promotion_Medium
    {

        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "Medium Type")]
        [StringLength(50, MinimumLength = 0)]
        public string medium_type { get; set; }

    }
}