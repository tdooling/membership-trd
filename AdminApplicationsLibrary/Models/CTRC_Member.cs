﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[user_id] [int] NULL,
    //[member_id] [int] NULL,
    //[first_name] [varchar](50) NOT NULL,
    //[last_name] [varchar](50) NOT NULL,
    //[ctrc_membership_type_id] [int] NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[is_active] [bit] NULL,
    //[simplified_review] [bit] NULL,

    [Table("ctrc_members")]
    [DisplayColumn("Id", "last_name")]
    public class CTRC_Member
    {
        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }

        [Display(Name = "User Id")]
        public int? user_id { get; set; }

        [Display(Name = "Member Id")]
        public int? member_id { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 1)]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string last_name { get; set; }

        [Display(Name = "Membership Type")]
        public int ctrc_membership_type_id { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [Display(Name = "Active")]
        public bool? is_active { get; set; }

        [Display(Name = "Simplified Review")]
        public bool? simplified_review { get; set; }

    }
}