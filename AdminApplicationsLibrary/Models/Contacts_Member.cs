﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace AdminApplicationsLibrary
{

    //[contact_id] [int] NOT NULL,
    //[member_id] [int] NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL DEFAULT ((0))

    [Table("contacts_members")]
    public class Contacts_Member
    {

        [Key]
        public int contact_id { get; set; }

        [Key]
        public int member_id { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at;

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

    }

}