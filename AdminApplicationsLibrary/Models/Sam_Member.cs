﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{
    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[user_id] [int] NULL DEFAULT (NULL),
    //[member_id] [int] NULL,
    //[first_name] [varchar](50) NOT NULL,
    //[last_name] [varchar](50) NOT NULL,
    //[created_at] [datetime] NOT NULL,
    //[updated_at] [datetime] NOT NULL,
    //[is_active] [bit] NULL DEFAULT ((1)),
    //[created_by] [int] NULL DEFAULT (NULL),
    //[updated_by] [int] NULL DEFAULT (NULL),
    //[lock_version] [int] NOT NULL DEFAULT ((0)),

    [Table("sam_members")]
    [DisplayColumn("Id", "last_name")]
    public class Sam_Member
    {
        [Key]
        [Display(Name="Id")]
        public int id { get; set; }

        public int? user_id { get; set; }

        public int? member_id { get; set; }

        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 1)]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string last_name { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Created")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        [Display(Name = "Last Updated")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Active")]
        public bool? is_active { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }

        [ScaffoldColumn(false)]
        public int lock_version { get; set; }

    }
}