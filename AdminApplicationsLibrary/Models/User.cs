﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdminApplicationsLibrary
{

    //[id] [int] IDENTITY(1,1) NOT NULL,
    //[user_name] [varchar](50) NOT NULL,
    //[password] [varchar](128) NOT NULL CONSTRAINT [DF_users_password]  DEFAULT ('aed32e7c6e59fcd5273421187338882fe5eb6eb1'),
    //[first_name] [varchar](50) NOT NULL,
    //[last_name] [varchar](50) NOT NULL,
    //[email] [varchar](100) NOT NULL,
    //[is_active] [bit] NOT NULL CONSTRAINT [DF__users__is_active__73852659]  DEFAULT ((1)),
    //[last_login_date] [datetime] NULL CONSTRAINT [DF__users__last_logi__74794A92]  DEFAULT (NULL),
    //[created_at] [datetime] NOT NULL CONSTRAINT [DF_users_created_at]  DEFAULT (getdate()),
    //[updated_at] [datetime] NOT NULL CONSTRAINT [DF_users_updated_at]  DEFAULT (getdate()),
    //[created_by] [int] NULL,
    //[updated_by] [int] NULL,
    //[lock_version] [int] NOT NULL CONSTRAINT [DF__users__lock_vers__7755B73D]  DEFAULT ((0)),
    //[middle_initial] [varchar](50) NULL,


    [Table("users")]
    [DisplayColumn("Id", "last_name")]
    public class User
    {

        [Key]
        [Display(Name = "Id")]
        public int id { get; set; }
        
        [Display(Name = "Username")]
        [StringLength(50, MinimumLength = 1)]
        public string user_name { get; set; }
        
        [Display(Name = "Password")]
        [StringLength(128, MinimumLength = 1)]
        public string password { get; set; }
        
        [Display(Name = "First Name")]
        [StringLength(50, MinimumLength = 1)]
        public string first_name { get; set; }
        
        [Display(Name = "Last Name")]
        [StringLength(50, MinimumLength = 1)]
        public string last_name { get; set; }
        
        [Display(Name = "Email")]
        [StringLength(100, MinimumLength = 1)]
        public string email { get; set; }
	    
        [Display(Name = "Is Active")]
        public Boolean is_active { get; set; }
	    
        [Display(Name = "Middle Initial")]
        [StringLength(50, MinimumLength = 0)]
        public string middle_initial { get; set; }

        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime? last_login_date { get; set; }

        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime created_at { get; set; }

        [DataType(DataType.DateTime)] // added by TRD 10/5/2015
        [DisplayFormat(DataFormatString = "{0:MMMM d, yyyy}")]
        public DateTime updated_at { get; set; }

        [Display(Name = "Created By")]
        public int? created_by { get; set; }

        [Display(Name = "Updated By")]
        public int? updated_by { get; set; }
        
        public int lock_version { get; set; }

    }
}
