﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class RoleController : Controller
    {

        private RoleRepository roleRepository; 
                
        public RoleController()
        {
            this.roleRepository = new RoleRepository();
        }

        public RoleController(RoleRepository roleRepository)
        {
            this.roleRepository = roleRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var roles = roleRepository.Fill().OrderBy(rank => rank.name);

             return View("Index",roles);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var role = roleRepository.Find(id);
            return View("Details",role);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Role() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Role role)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    role.id = roleRepository.Create(role);
                    return RedirectToAction("Index");
                }
                return View("Create", role);
            }
            catch
            {
                return View("Create", role);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var role = roleRepository.Find(id);
                if (role != null)
                {
                    return View("Edit", role);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var role = roleRepository.Find(id);
                if (role != null)
                {
                    if (TryUpdateModel(role))
                    {
                        try
                        {
                            roleRepository.Update(id, role);
                            return RedirectToAction("Details", "Role", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", role);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var role = roleRepository.Find(id);
                if (role != null)
                {
                    return View("Delete", role);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var role = roleRepository.Find(id);
                if (role != null)
                    {
                        try
                            {
                                roleRepository.Delete(id, role);
                                return RedirectToAction("Index", "Role");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", role);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
