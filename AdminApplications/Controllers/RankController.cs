﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class RankController : Controller
    {

        private RankRepository rankRepository; 
                
        public RankController()
        {
            this.rankRepository = new RankRepository();
        }

        public RankController(RankRepository rankRepository)
        {
            this.rankRepository = rankRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var ranks = rankRepository.Fill().OrderBy(rank => rank.name);

             return View("Index",ranks);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var rank = rankRepository.Find(id);
            return View(rank);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Rank() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Rank rank)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    rank.id = rankRepository.Create(rank);
                    return RedirectToAction("Index");
                }
                return View("Create", rank);
            }
            catch
            {
                return View("Create", rank);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var rank = rankRepository.Find(id);
                if (rank != null)
                {
                    return View("Edit", rank);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var rank = rankRepository.Find(id);
                if (rank != null)
                {
                    if (TryUpdateModel(rank))
                    {
                        try
                        {
                            rankRepository.Update(id, rank);
                            return RedirectToAction("Details", "Rank", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", rank);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var rank = rankRepository.Find(id);
                if (rank != null)
                {
                    return View("Delete", rank);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var rank = rankRepository.Find(id);
                if (rank != null)
                    {
                        try
                            {
                                rankRepository.Delete(id, rank);
                                return RedirectToAction("Index", "Rank");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", rank);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
