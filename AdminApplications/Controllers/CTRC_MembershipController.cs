﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class CTRC_MembershipController : Controller
    {

        private CTRC_MembershipRepository ctrc_MembershipRepository; 
                
        public CTRC_MembershipController()
        {
            this.ctrc_MembershipRepository = new CTRC_MembershipRepository();
        }

        public CTRC_MembershipController(CTRC_MembershipRepository ctrc_MembershipRepository)
        {
            this.ctrc_MembershipRepository = ctrc_MembershipRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var ctrc_Membership = ctrc_MembershipRepository.Fill().OrderByDescending(ctrc => ctrc.term_start_date);

             return View("Index",ctrc_Membership);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var ctrc_Membership = ctrc_MembershipRepository.Find(id);
            return View(ctrc_Membership);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new CTRC_Membership() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(CTRC_Membership ctrc_Membership)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ctrc_Membership.id = ctrc_MembershipRepository.Create(ctrc_Membership);
                    return RedirectToAction("Index","CTRC_Membership");
                }
                return View("Create", ctrc_Membership);
            }
            catch
            {
                return View("Create", ctrc_Membership);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var ctrc_membership = ctrc_MembershipRepository.Find(id);
                if (ctrc_membership != null)
                {
                    return View("Edit", ctrc_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var ctrc_membership = ctrc_MembershipRepository.Find(id);
                if (ctrc_membership != null)
                {
                    if (TryUpdateModel(ctrc_membership))
                    {
                        try
                        {
                            ctrc_MembershipRepository.Update(id, ctrc_membership);
                            return RedirectToAction("Details", "CTRC_Membership", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", ctrc_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var ctrc_membership = ctrc_MembershipRepository.Find(id);
                if (ctrc_membership != null)
                {
                    return View("Delete", ctrc_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var ctrc_membership = ctrc_MembershipRepository.Find(id);
                if (ctrc_membership != null)
                    {
                        try
                            {
                                ctrc_MembershipRepository.Delete(id, ctrc_membership);
                                return RedirectToAction("Index", "CTRC_Membership");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", ctrc_membership);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
