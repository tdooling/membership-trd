﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;

namespace AdminApplications.Controllers
{
    [Authorize]
    public class MembershipController : Controller
    {
        private MembershipRepository membershipRepository; 
                
        public MembershipController()
        {
            this.membershipRepository = new MembershipRepository();
        }

        public MembershipController(MembershipRepository membershipRepository)
        {
            this.membershipRepository = membershipRepository;
        }

        // GET: Membership
        public ActionResult Index()
        {
            var memberships = membershipRepository.Fill();
            return View("Index", memberships);
        }

        // GET: Membership/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var membership = membershipRepository.Find(id);
                if (membership != null)
                {
                    return View("Edit", membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Membership/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var membership = membershipRepository.Find(id);
                if (membership != null)
                {
                    if (TryUpdateModel(membership))
                    {
                        try
                        {
                            membershipRepository.Update(id, membership);
                            Object obj = TempData["memberId"];
                            TempData["memberId"] = null;
                            int memberId = 0;
                            if (obj==null)
	                            {
                                    return RedirectToAction("Details", "Membership", new { id = id });
	                            }
                            if (int.TryParse(obj.ToString(), out memberId))
	                            {
                                    if (memberId>0)
                                    {
                                        return RedirectToAction("Edit", "MemberList", new { id = memberId });
                                    }
	                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}