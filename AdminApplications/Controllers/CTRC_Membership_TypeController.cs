﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class CTRC_Membership_TypeController : Controller
    {

        private CTRC_Membership_TypeRepository ctrc_Membership_TypeRepository; 
                
        public CTRC_Membership_TypeController()
        {
            this.ctrc_Membership_TypeRepository = new CTRC_Membership_TypeRepository();
        }

        public CTRC_Membership_TypeController(CTRC_Membership_TypeRepository ctrc_Membership_TypeRepository)
        {
            this.ctrc_Membership_TypeRepository = ctrc_Membership_TypeRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var ctrc_membership_types = ctrc_Membership_TypeRepository.Fill().OrderBy(ctrc => ctrc.membership_type);

             return View("Index",ctrc_membership_types);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var ctrc_membership_types = ctrc_Membership_TypeRepository.Find(id);
            return View(ctrc_membership_types);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new CTRC_Membership_Type() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(CTRC_Membership_Type ctrc_membership_types)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ctrc_membership_types.id = ctrc_Membership_TypeRepository.Create(ctrc_membership_types);
                    return RedirectToAction("Index");
                }
                return View("Create", ctrc_membership_types);
            }
            catch
            {
                return View("Create", ctrc_membership_types);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var ctrc_membership_types = ctrc_Membership_TypeRepository.Find(id);
                if (ctrc_membership_types != null)
                {
                    return View("Edit", ctrc_membership_types);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var ctrc_membership_types = ctrc_Membership_TypeRepository.Find(id);
                if (ctrc_membership_types != null)
                {
                    if (TryUpdateModel(ctrc_membership_types))
                    {
                        try
                        {
                            ctrc_Membership_TypeRepository.Update(id, ctrc_membership_types);
                            return RedirectToAction("Details", "CTRC_Membership_Type", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", ctrc_membership_types);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var ctrc_membership_types = ctrc_Membership_TypeRepository.Find(id);
                if (ctrc_membership_types != null)
                {
                    return View("Delete", ctrc_membership_types);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var ctrc_membership_types = ctrc_Membership_TypeRepository.Find(id);
                if (ctrc_membership_types != null)
                    {
                        try
                            {
                                ctrc_Membership_TypeRepository.Delete(id, ctrc_membership_types);
                                return RedirectToAction("Index", "CTRC_Membership_Type");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", ctrc_membership_types);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
