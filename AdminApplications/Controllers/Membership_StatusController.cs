﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AdminApplications.DAL;
using AdminApplicationsLibrary;

namespace AdminApplications.Controllers
{

    [Authorize]
    public class Membership_StatusController : Controller
    {

        private Membership_StatusRepository member_StatuseRepository; 
                
        public Membership_StatusController()
        {
            this.member_StatuseRepository = new Membership_StatusRepository();
        }

        public Membership_StatusController(Membership_StatusRepository member_StatuseRepository)
        {
            this.member_StatuseRepository = member_StatuseRepository;
        }


        // GET: Member
        public ActionResult Index()
        {
            var membership_Statuses = member_StatuseRepository.Fill().OrderBy(status => status.name);

            return View("Index", membership_Statuses);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var membership_Status = member_StatuseRepository.Find(id);
            return View(membership_Status);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Membership_Status() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Membership_Status membership_Statuses)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    membership_Statuses.id = member_StatuseRepository.Create(membership_Statuses);
                    return RedirectToAction("Index");
                }
                return View("Create", membership_Statuses);
            }
            catch
            {
                return View("Create", membership_Statuses);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var membership_Statuses = member_StatuseRepository.Find(id);
                if (membership_Statuses != null)
                {
                    return View("Edit", membership_Statuses);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var membership_Statuses = member_StatuseRepository.Find(id);
                if (membership_Statuses != null)
                {
                    if (TryUpdateModel(membership_Statuses))
                    {
                        try
                        {
                            member_StatuseRepository.Update(id, membership_Statuses);
                            return RedirectToAction("Details", "Member_Status", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", membership_Statuses);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var membership_Statuses = member_StatuseRepository.Find(id);
                if (membership_Statuses != null)
                {
                    return View("Delete", membership_Statuses);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var membership_Statuses = member_StatuseRepository.Find(id);
                if (membership_Statuses != null)
                {
                    try
                    {
                        member_StatuseRepository.Delete(id, membership_Statuses);
                        return RedirectToAction("Index", "Member_Status");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", membership_Statuses);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
