﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class CRAC_MembershipController : Controller
    {

        private CRAC_MembershipRepository crac_MembershipRepository; 
                
        public CRAC_MembershipController()
        {
            this.crac_MembershipRepository = new CRAC_MembershipRepository();
        }

        public CRAC_MembershipController(CRAC_MembershipRepository crac_MembershipRepository)
        {
            this.crac_MembershipRepository = crac_MembershipRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var crac_membership = crac_MembershipRepository.Fill().OrderByDescending(ctrc => ctrc.term_start_date);

            return View("Index", crac_membership);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var crac_membership = crac_MembershipRepository.Find(id);
            return View(crac_membership);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new CRAC_Membership() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(CRAC_Membership crac_membership)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    crac_membership.id =  crac_MembershipRepository.Create(crac_membership);
                    return RedirectToAction("Index");
                }
                return View("Create", crac_membership);
            }
            catch
            {
                return View("Create", crac_membership);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var crac_membership = crac_MembershipRepository.Find(id);
                if (crac_membership != null)
                {
                    return View("Edit", crac_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var crac_membership = crac_MembershipRepository.Find(id);
                if (crac_membership != null)
                {
                    if (TryUpdateModel(crac_membership))
                    {
                        try
                        {
                            crac_MembershipRepository.Update(id, crac_membership);
                            return RedirectToAction("Details", "CRAC_Membership", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", crac_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var crac_membership = crac_MembershipRepository.Find(id);
                if (crac_membership != null)
                {
                    return View("Delete", crac_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var crac_membership = crac_MembershipRepository.Find(id);
                if (crac_membership != null)
                    {
                        try
                            {
                                crac_MembershipRepository.Delete(id, crac_membership);
                                return RedirectToAction("Index", "CRAC_Membership");
                            }
                        catch (Exception ex)
                            {
                            }
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
