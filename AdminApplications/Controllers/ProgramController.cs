﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class ProgramController : Controller
    {

        private ProgramRepository programRepository; 
                
        public ProgramController()
        {
            this.programRepository = new ProgramRepository();
        }

        public ProgramController(ProgramRepository programRepository)
        {
            this.programRepository = programRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var programs = programRepository.Fill().OrderBy(section => section.name);

             return View("Index",programs);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var program = programRepository.Find(id);
            return View("Details", program);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Program() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Program program)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    program.id = programRepository.Create(program);
                    return RedirectToAction("Index");
                }
                return View("Create", program);
            }
            catch
            {
                return View("Create", program);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var program = programRepository.Find(id);
                if (program != null)
                {
                    return View("Edit", program);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var program = programRepository.Find(id);
                if (program != null)
                {
                    if (TryUpdateModel(program))
                    {
                        try
                        {
                            programRepository.Update(id, program);
                            return RedirectToAction("Details", "Program", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", program);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var program = programRepository.Find(id);
                if (program != null)
                {
                    return View("Delete", program);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var program = programRepository.Find(id);
                if (program != null)
                    {
                        try
                            {
                                programRepository.Delete(id, program);
                                return RedirectToAction("Index", "Program");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", program);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
