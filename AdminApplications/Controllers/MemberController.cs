﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplications.DAL;
using System.Net;
using AdminApplicationsLibrary;


namespace AdminApplications.Controllers
{
    [Authorize]
    public class MemberController : Controller
    {
        private MemberRepository memberRepository; 
                
        public MemberController()
        {
            this.memberRepository = new MemberRepository();
        }

        public MemberController(MemberRepository memberRepository)
        {
            this.memberRepository = memberRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var members = memberRepository.Fill();
            return View("Index", members);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var member = memberRepository.Find(id);
            return View("Details", member);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Member() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Member member)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    member.id = memberRepository.Create(member);
                    return RedirectToAction("Index", "Member");
                }
                return View("Create", member);
            }
            catch
            {
                return View("Create", member);
            }
        }

        // GET: Member/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id != null)
                {
                    var result = memberRepository.Find(id);
                    if (result != null)
                        {
                            return View(result);
                        }
                    return HttpNotFound();
                }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id!=null)
            {
                var member = memberRepository.Find(id);
                if (member!=null)
                    {
                        if (TryUpdateModel(member))
                            {
                                try
                                {
                                    memberRepository.Update(id, member);
                                    return RedirectToAction("Edit", "MemberList", new { id = id });
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        return View("Edit",member);
                    }
                return HttpNotFound();
            }

            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var member = memberRepository.Find(id);
                if (member != null)
                {
                    return View("Delete", member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var member = memberRepository.Find(id);
                if (member != null)
                {
                    try
                    {
                        memberRepository.Delete(id, member);
                        return RedirectToAction("Index", "MemberList");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    
    }
}

