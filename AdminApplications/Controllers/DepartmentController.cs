﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;

namespace AdminApplications.Controllers
{

    [Authorize]
    public class DepartmentController : Controller
    {
        private DepartmentRepository departmentRepository; 
                
        public DepartmentController()
        {
            this.departmentRepository = new DepartmentRepository();
        }

        public DepartmentController(DepartmentRepository departmentRepository)
        {
            this.departmentRepository = departmentRepository;
        }

        // GET: Department
        public ActionResult Index()
        {
            var result = departmentRepository.Fill();
            return View(result);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var department = departmentRepository.Find(id);
            return View("Details",department);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View(new Department() {created_at=DateTime.Now,updated_at=DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Department department)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    department.id = departmentRepository.Create(department);
                    return RedirectToAction("Index", "Department");
                }
                return View("Create", department);
            }
            catch
            {
                return View("Create", department);
            }
        }

        // GET: Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id != null)
                {
                    var result = departmentRepository.Find(id);
                    if (result != null)
                        {
                            return View("Edit", result);
                        }
                    return HttpNotFound();
                }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id!=null)
            {
                var department = departmentRepository.Find(id);
                if (department!=null)
                    {
                        if (TryUpdateModel(department))
                            {
                                try
                                {
                                    departmentRepository.Update(id, department);
                                    return RedirectToAction("Details", "Department", new { id = id });
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        return View("Edit",department);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var department = departmentRepository.Find(id);
                if (department != null)
                {
                    return View("Delete", department);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var department = departmentRepository.Find(id);
                if (department != null)
                {
                    try
                    {
                        departmentRepository.Delete(id, department);
                        return RedirectToAction("Index", "Department");
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
