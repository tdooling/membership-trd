﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;

namespace AdminApplications.Controllers
{

    [Authorize]
    public class Member_CommunicationController : Controller
    {
        private Member_CommunicationRepository member_CommunicationRepository; 
                
        public Member_CommunicationController()
        {
            this.member_CommunicationRepository = new Member_CommunicationRepository();
        }

        public Member_CommunicationController(Member_CommunicationRepository member_Communication)
        {
            this.member_CommunicationRepository = member_Communication;
        }

        // GET: Department
        public ActionResult Index()
        {
            var result = member_CommunicationRepository.Fill();
            return View(result);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var member_Communication = member_CommunicationRepository.Find(id);
            return View(member_Communication);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Member_Communication());
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Member_Communication member_Communication)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    member_Communication.id = member_CommunicationRepository.Create(member_Communication);
                    return RedirectToAction("Index", "Member_Communication");
                }
                return View("Create", member_Communication);
            }
            catch
            {
                return View("Create", member_Communication);
            }
        }

        // GET: Department/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id != null)
                {
                    var member_Communication = member_CommunicationRepository.Find(id);
                    if (member_Communication != null)
                        {
                            return View("Edit",member_Communication);
                        }
                    return HttpNotFound();
                }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Edit/5
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id!=null)
            {
                var member_Communication = member_CommunicationRepository.Find(id);
                if (member_Communication!=null)
                    {
                        if (TryUpdateModel(member_Communication))
                            {
                                try
                                {
                                    member_CommunicationRepository.Update(id, member_Communication);
                                    return RedirectToAction("Details", "Member_Communication", new { id = id });
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        return View("Edit",member_Communication);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var member_Communication = member_CommunicationRepository.Find(id);
                if (member_Communication != null)
                {
                    return View("Delete", member_Communication);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var member_Communication = member_CommunicationRepository.Find(id);
                if (member_Communication != null)
                {
                    try
                    {
                        member_CommunicationRepository.Delete(id, member_Communication);
                        return RedirectToAction("Index", "Member_Communication");
                    }
                    catch (Exception ex)
                    {
                    }
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }
    }
}
