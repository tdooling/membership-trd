﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using AdminApplications.Models;
using System.Net;


namespace AdminApplications.Controllers
{
    [Authorize]
    public class MemberListController : Controller
    {
        private MemberListViewModelRepository memberListViewModelRepository;
       
        public MemberListController()
        {
            this.memberListViewModelRepository = new MemberListViewModelRepository();
        }

        public MemberListController(MemberListViewModelRepository memberListViewModelRepository)
        {
            this.memberListViewModelRepository = memberListViewModelRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var memberListViewModels = memberListViewModelRepository.Fill().OrderBy(ml=>ml.last_name);
            ViewBag.Title = "View Members";
            return View("Index", memberListViewModels);
        }

        // GET: Member
        public ActionResult Leadership()
        {
            var memberListViewModels = memberListViewModelRepository.GetAllLeadership().OrderBy(ml => ml.last_name);
            ViewBag.Title = "View Leadership"; 
            return View("Index", memberListViewModels);
        }

        // GET: Member
        public ActionResult ResearchInterest()
        {
            var memberListViewModels = memberListViewModelRepository.GetAllResearch().OrderBy(ml => ml.last_name);
            ViewBag.Title = "View Research Interests";
            return View("Index", memberListViewModels);
        }

        // GET: Member
        public ActionResult Edit(int? id)
        {
            if (id != null)
                {
                    var member = new MemberListEditViewModel(id);
                    if (member != null)
                        {
                            TempData["memberId"] = id;
                            return View("Edit", member);
                        }
                    return HttpNotFound();
                }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }


    }
}