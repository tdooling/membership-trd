﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;

namespace AdminApplications.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private UserRepository userRepository;

        public UserController()
        {
            this.userRepository = new UserRepository();
        }

        public UserController(UserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var users = userRepository.Fill().OrderBy(us => us.last_name);

            return View("Index", users);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var user = userRepository.Find(id);
            return View(user);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new User() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(User user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    user.id = userRepository.Create(user);
                    return RedirectToAction("Index");
                }
                return View("Create", user);
            }
            catch
            {
                return View("Create", user);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var user = userRepository.Find(id);
                if (user != null)
                {
                    return View("Edit", user);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var user = userRepository.Find(id);
                if (user != null)
                {
                    if (TryUpdateModel(user))
                    {
                        try
                        {
                            userRepository.Update(id, user);
                            return RedirectToAction("Details", "User", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", user);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var user = userRepository.Find(id);
                if (user != null)
                {
                    return View("Delete", user);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var user = userRepository.Find(id);
                if (user != null)
                {
                    try
                    {
                        userRepository.Delete(id, user);
                        return RedirectToAction("Index", "User");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", user);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
