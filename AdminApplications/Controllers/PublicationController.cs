﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AdminApplicationsLibrary;
using AdminApplications.Helpers;
using AdminApplications.DAL;

namespace AdminApplications.Controllers
{

    public class PublicationController : ApiController
    {
        private PublicationRepository publicationRepository;
        private Members_PublicationRepository members_PublicationRepository;

        public PublicationController()
        {
            publicationRepository = new PublicationRepository();
            members_PublicationRepository = new Members_PublicationRepository();
        }

        public PublicationController(PublicationRepository publicationRepository, Members_PublicationRepository members_PublicationRepository)
        {
            this.publicationRepository = publicationRepository;
            this.members_PublicationRepository = members_PublicationRepository;
        }

        // GET api/<controller>
        [HttpGet]
        public IEnumerable<Publication> Get()
        {
            var publications = publicationRepository.Fill();

            return publications;
        }

        // GET api/<controller>/5
        [HttpGet]
        [ActionName("Get")]
        public Publication Get(int id)
        {
            return publicationRepository.Find(id);
        }

        // GET api/<controller>/5
        [HttpGet]
        [ActionName("ListPublications")]
        public IEnumerable<Publication> GetByMemberId(int id)
        {
            var publications = from data in members_PublicationRepository.FillByMemberId(id).ToList()
                               select data.publication_id ?? 0;
            var member_publications = publicationRepository.Fill(publications);
            //if (member_publications==null || member_publications.Count()<1)
            //{
            //    member_publications = null;
            //    var pubs = new List<Publication>();
            //    pubs.Add(new Publication() { title = "There are no publications for this member.", publication_year = "", author_names = "", journal_title = "" });
            //    member_publications = pubs.ToList();
            //}
            return member_publications;
        }


        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]Publication publication)
        {
            if (publication != null)
            {
                publicationRepository.Create(publication);
            }
        }

        // PUT api/<controller>/5
        [HttpPut]
        public void Put(int id, [FromBody]Publication publication)
        {
            if (publication != null && publication.id == id)
            {
                if (publicationRepository.Find(id) != null)
                {
                    publicationRepository.Update(publication.id, publication);
                }
            }
        }

        // DELETE api/<controller>/5
        [HttpDelete]
        public void Delete(int id)
        {
            try
            {
                var publication = publicationRepository.Find(id);
                if (publication != null)
                {
                    publicationRepository.Delete(publication.id, publication);
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
