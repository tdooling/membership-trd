﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;

namespace AdminApplications.Controllers
{
    [Authorize]
    public class UniversityAppointmentController : Controller
    {

        private UniversityAppointmentRepository universityAppointmentRepository;

        public UniversityAppointmentController()
        {
            this.universityAppointmentRepository = new UniversityAppointmentRepository();
        }

        public UniversityAppointmentController(UniversityAppointmentRepository universityAppointmentRepository)
        {
            this.universityAppointmentRepository = universityAppointmentRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var universityAppointment = universityAppointmentRepository.Fill().OrderByDescending(univ => univ.start_date);

            return View("Index", universityAppointment);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var universityAppointment = universityAppointmentRepository.Find(id);
            return View(universityAppointment);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new University_Appointment() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(University_Appointment universityAppointment)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    universityAppointment.id = universityAppointmentRepository.Create(universityAppointment);
                    return RedirectToAction("Index");
                }
                return View("Create", universityAppointment);
            }
            catch
            {
                return View("Create", universityAppointment);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var universityAppointment = universityAppointmentRepository.Find(id);
                if (universityAppointment != null)
                {
                    return View("Edit", universityAppointment);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var universityAppointment = universityAppointmentRepository.Find(id);
                if (universityAppointment != null)
                {
                    if (TryUpdateModel(universityAppointment))
                    {
                        try
                        {
                            universityAppointmentRepository.Update(id, universityAppointment);
                            return RedirectToAction("Edit", "MemberList", new { id = universityAppointment.member_id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", universityAppointment);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var universityAppointment = universityAppointmentRepository.Find(id);
                if (universityAppointment != null)
                {
                    return View("Delete", universityAppointment);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var universityAppointment = universityAppointmentRepository.Find(id);
                if (universityAppointment != null)
                {
                    try
                    {
                        universityAppointmentRepository.Delete(id, universityAppointment);
                        return RedirectToAction("Index", "UniversityAppointment");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", universityAppointment);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
