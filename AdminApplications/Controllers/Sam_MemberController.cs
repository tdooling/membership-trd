﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class Sam_MemberController : Controller
    {
        private Sam_MemberRepository sam_MemberRepository;

        public Sam_MemberController()
        {
            this.sam_MemberRepository = new Sam_MemberRepository();
        }

        public Sam_MemberController(Sam_MemberRepository sam_MemberRepository)
        {
            this.sam_MemberRepository = sam_MemberRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var sam_Member = sam_MemberRepository.Fill().OrderBy(us => us.last_name);

            return View("Index", sam_Member);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var sam_Member = sam_MemberRepository.Find(id);
            return View("Details", sam_Member);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Sam_Member() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Sam_Member sam_Member)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    sam_Member.id = sam_MemberRepository.Create(sam_Member);
                    return RedirectToAction("Index");
                }
                return View("Create", sam_Member);
            }
            catch
            {
                return View("Create", sam_Member);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var sam_Member = sam_MemberRepository.Find(id);
                if (sam_Member != null)
                {
                    return View("Edit", sam_Member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var sam_Member = sam_MemberRepository.Find(id);
                if (sam_Member != null)
                {
                    if (TryUpdateModel(sam_Member))
                    {
                        try
                        {
                            sam_MemberRepository.Update(id, sam_Member);
                            return RedirectToAction("Details", "SAM_Member", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", sam_Member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var sam_Member = sam_MemberRepository.Find(id);
                if (sam_Member != null)
                {
                    return View("Delete", sam_Member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var sam_Member = sam_MemberRepository.Find(id);
                if (sam_Member != null)
                {
                    try
                    {
                        sam_MemberRepository.Delete(id, sam_Member);
                        return RedirectToAction("Index", "SAM_Member");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", sam_Member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}

