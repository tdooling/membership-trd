﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;

namespace AdminApplications.Controllers
{
    [Authorize]
    public class Member_Promotion_MediumController : Controller
    {

        private Member_Promotion_MediumRepository member_Promotion_MediumRepository; 
                
        public Member_Promotion_MediumController()
        {
            this.member_Promotion_MediumRepository = new Member_Promotion_MediumRepository();
        }

        public Member_Promotion_MediumController(Member_Promotion_MediumRepository member_Promotion_MediumRepository)
        {
            this.member_Promotion_MediumRepository = member_Promotion_MediumRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var member_Promotion_Medium = member_Promotion_MediumRepository.Fill().OrderBy(ctrc => ctrc.medium_type);

            return View("Index", member_Promotion_Medium);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var member_Promotion_Medium = member_Promotion_MediumRepository.Find(id);
            return View(member_Promotion_Medium);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Member_Promotion_Medium());
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Member_Promotion_Medium member_Promotion_Medium)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    member_Promotion_Medium.id = member_Promotion_MediumRepository.Create(member_Promotion_Medium);
                    return RedirectToAction("Index", "Member_Promotion_Medium");
                }
                return View("Create", member_Promotion_Medium);
            }
            catch
            {
                return View("Create", member_Promotion_Medium);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var member_Promotion_Medium = member_Promotion_MediumRepository.Find(id);
                if (member_Promotion_Medium != null)
                {
                    return View("Edit", member_Promotion_Medium);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var member_Promotion_Medium = member_Promotion_MediumRepository.Find(id);
                if (member_Promotion_Medium != null)
                {
                    if (TryUpdateModel(member_Promotion_Medium))
                    {
                        try
                        {
                            member_Promotion_MediumRepository.Update(id, member_Promotion_Medium);
                            return RedirectToAction("Details", "Member_Promotion_Medium", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", member_Promotion_Medium);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var member_Promotion_Medium = member_Promotion_MediumRepository.Find(id);
                if (member_Promotion_Medium != null)
                {
                    return View("Delete", member_Promotion_Medium);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var member_Promotion_Medium = member_Promotion_MediumRepository.Find(id);
                if (member_Promotion_Medium != null)
                    {
                        try
                            {
                                member_Promotion_MediumRepository.Delete(id, member_Promotion_Medium);
                                return RedirectToAction("Index", "Member_Promotion_Medium");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", member_Promotion_Medium);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
