﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;

namespace AdminApplications.Controllers
{

    [Authorize]
    public class CTRCMemberController : Controller
    {
        private CTRC_MemberRepository CTRC_MemberRepository; 
                
        public CTRCMemberController()
        {
            this.CTRC_MemberRepository = new CTRC_MemberRepository();
        }

        public CTRCMemberController(CTRC_MemberRepository CTRC_MemberRepository)
        {
            this.CTRC_MemberRepository = CTRC_MemberRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var ctrc_members = CTRC_MemberRepository.Fill().Where(mem => mem.is_active??false);

             return View("Index",ctrc_members);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var ctrc_member = CTRC_MemberRepository.Find(id);
            return View(ctrc_member);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new CTRC_Member() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(CTRC_Member ctrc_member)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    ctrc_member.id = CTRC_MemberRepository.Create(ctrc_member);
                    return RedirectToAction("Index", "CTRC_Member");
                }
                return RedirectToAction("Create", ctrc_member);
            }
            catch
            {
                return View("Create", ctrc_member);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var ctrc_member = CTRC_MemberRepository.Find(id);
                if (ctrc_member != null)
                {
                    return View(ctrc_member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var ctrc_member = CTRC_MemberRepository.Find(id);
                if (ctrc_member != null)
                {
                    if (TryUpdateModel(ctrc_member))
                    {
                        try
                        {
                            CTRC_MemberRepository.Update(id, ctrc_member);
                            return RedirectToAction("Details", "CTRC_Member", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", ctrc_member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var ctrc_member = CTRC_MemberRepository.Find(id);
                if (ctrc_member != null)
                {
                    return View(ctrc_member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var ctrc_member = CTRC_MemberRepository.Find(id);
                if (ctrc_member != null)
                {
                    try
                        {
                            CTRC_MemberRepository.Delete(id, ctrc_member);
                            return RedirectToAction("Index", "CTRCMember");
                        }
                    catch (Exception ex)
                        {
                        }
                    return View("Delete", ctrc_member);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
