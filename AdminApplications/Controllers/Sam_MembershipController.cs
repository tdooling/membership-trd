﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class Sam_MembershipController : Controller
    {
        private Sam_MembershipRepository sam_MembershipRepository;

        public Sam_MembershipController()
        {
            this.sam_MembershipRepository = new Sam_MembershipRepository();
        }

        public Sam_MembershipController(Sam_MembershipRepository sam_MembershipRepository)
        {
            this.sam_MembershipRepository = sam_MembershipRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var sam_Membership = sam_MembershipRepository.Fill().OrderByDescending(us => us.term_start_date);

            return View("Index", sam_Membership);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var sam_Membership = sam_MembershipRepository.Find(id);
            return View("Details", sam_Membership);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Sam_Member() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Sam_Membership sam_Membership)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    sam_Membership.id = sam_MembershipRepository.Create(sam_Membership);
                    return RedirectToAction("Index");
                }
                return View("Create", sam_Membership);
            }
            catch
            {
                return View("Create", sam_Membership);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id != null)
            {
                var sam_Membership = sam_MembershipRepository.Find(id);
                if (sam_Membership != null)
                {
                    return View("Edit", sam_Membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
            {
                var sam_Membership = sam_MembershipRepository.Find(id);
                if (sam_Membership != null)
                {
                    if (TryUpdateModel(sam_Membership))
                    {
                        try
                        {
                            sam_MembershipRepository.Update(id, sam_Membership);
                            return RedirectToAction("Details", "SAM_Membership", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", sam_Membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var sam_Membership = sam_MembershipRepository.Find(id);
                if (sam_Membership != null)
                {
                    return View("Delete", sam_Membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var sam_Membership = sam_MembershipRepository.Find(id);
                if (sam_Membership != null)
                {
                    try
                    {
                        sam_MembershipRepository.Delete(id, sam_Membership);
                        return RedirectToAction("Index", "SAM_Membership");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", sam_Membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}

