﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class CEC_MembershipController : Controller
    {

        private CEC_MembershipRepository cec_MembershipRepository; 
                
        public CEC_MembershipController()
        {
            this.cec_MembershipRepository = new CEC_MembershipRepository();
        }

        public CEC_MembershipController(CEC_MembershipRepository cec_MembershipRepository)
        {
            this.cec_MembershipRepository = cec_MembershipRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var cec_membership = cec_MembershipRepository.Fill().OrderByDescending(cec => cec.term_start_date);

            return View("Index", cec_membership);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var cec_membership = cec_MembershipRepository.Find(id);
            return View("Details", cec_membership);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new CEC_Membership() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(CEC_Membership cec_membership)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    cec_membership.id = cec_MembershipRepository.Create(cec_membership);
                    return RedirectToAction("Index");
                }
                return View("Create", cec_membership);
            }
            catch
            {
                return View("Create", cec_membership);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var cec_membership = cec_MembershipRepository.Find(id);
                if (cec_membership != null)
                {
                    return View("Edit", cec_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var cec_membership = cec_MembershipRepository.Find(id);
                if (cec_membership != null)
                {
                    if (TryUpdateModel(cec_membership))
                    {
                        try
                        {
                            cec_MembershipRepository.Update(id, cec_membership);
                            return RedirectToAction("Details", "CEC_Membership", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", cec_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var cec_membership = cec_MembershipRepository.Find(id);
                if (cec_membership != null)
                {
                    return View("Delete", cec_membership);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var cec_membership = cec_MembershipRepository.Find(id);
                if (cec_membership != null)
                    {
                        try
                            {
                                cec_MembershipRepository.Delete(id, cec_membership);
                                return RedirectToAction("Index", "CEC_Membership");
                            }
                        catch (Exception ex)
                            {
                            }
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
