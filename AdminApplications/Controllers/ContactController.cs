﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Dapper;
using System.IO;
using System.Data;
using System.Collections;
using System.Reflection;
using System.Dynamic;
using System.ComponentModel;
using Microsoft.CSharp.RuntimeBinder;
using System.Data.Common;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Data.Entity;
using System.Net;

namespace AdminApplications.Controllers
{
    [Authorize]
    public class ContactController : Controller
    {
        private ContactRepository contactRepository;
        private Contacts_MemberRepository contactsMemberRepository;

        public ContactController()
        {
            this.contactRepository = new ContactRepository();
            this.contactsMemberRepository = new Contacts_MemberRepository();
        }

        public ContactController(ContactRepository contactRepository, Contacts_MemberRepository contactsMemberRepository)
        {
            this.contactRepository = contactRepository;
            this.contactsMemberRepository = contactsMemberRepository;
        }

        // GET: Contact
        public ActionResult Index()
        {
            int memberId = 0;
            Object obj = Session["memberId"];
            TempData["memberId"] = null;
            if (obj != null)
            {
                if (int.TryParse(obj.ToString(), out memberId))
                {
                    if (memberId > 0)
                    {
                        Session["memberId"] = null;
                        return RedirectToAction("Edit", "MemberList", new { id = memberId });
                    }
                }
            }
            Session["memberId"] = null;
            var contacts = contactRepository.Fill();
            return View("Index", contacts);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var contact = contactRepository.Find(id);
            return View("Details", contact);
        }

        // GET: Contact/Edit/5
        public ActionResult Edit(int? id)
        {
            var obj = TempData["memberId"];
            ViewData["memberId"] = obj;
            TempData["memberId"] = null;
            if (id != null)
                {
                    var result = contactRepository.Find(id);
                    if (result != null)
                        {
                            return View("Edit", result);
                        }
                    return HttpNotFound();
                }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Contact() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        [HttpPost, ActionName("Create")]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(Contact contact)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    contact.id = contactRepository.Create(contact);
                    return RedirectToAction("Index");
                }
                return View("Create", contact);
            }
            catch
            {
                return View("Create", contact);
            }

        }

        // POST: Contact/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        //[ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id)
        {
            if (id != null)
                {
                    var contact = contactRepository.Find(id);
                    if (contact != null)
                        {
                            if (TryUpdateModel(contact))
                                {
                                    try
                                        {
                                            contactRepository.Update(id, contact);

                                            int memberId = 0;
                                            Object obj = Session["memberId"];
                                            if (obj!=null)
                                                {
                                                    if (int.TryParse(obj.ToString(), out memberId))
                                                        {
                                                            if (memberId>0)
                                                                {
                                                                    Session["memberId"] = null;
                                                                    return RedirectToAction("Edit", "MemberList", new { id = memberId });
                                                                }
                                                        }
                                                }
                                            Session["memberId"] = null;
                                            return RedirectToAction("Details", "Contact", new { id = id });
                                        }
                                    catch (Exception ex)
                                        {
                                        }
                                }
                            Session["memberId"] = null;
                            return View("Edit", contact);
                        }
                    Session["memberId"] = null;
                    return HttpNotFound();
                }
            Session["memberId"] = null;
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var contact = contactRepository.Find(id);
                if (contact != null)
                {
                    return View("Delete", contact);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var contact = contactRepository.Find(id);
                if (contact != null)
                {
                    try
                    {
                        contactRepository.Delete(id, contact);
                        return RedirectToAction("Index", "Contact");
                    }
                    catch (Exception ex)
                    {
                    }
                    return View("Delete", contact);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}