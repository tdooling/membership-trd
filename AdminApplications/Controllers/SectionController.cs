﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class SectionController : Controller
    {

        private SectionRepository sectionRepository; 
                
        public SectionController()
        {
            this.sectionRepository = new SectionRepository();
        }

        public SectionController(SectionRepository sectionRepository)
        {
            this.sectionRepository = sectionRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var sections = sectionRepository.Fill().OrderBy(section => section.name);

             return View("Index",sections);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var section = sectionRepository.Find(id);
            return View("Details", section);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Section() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Section section)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    section.id = sectionRepository.Create(section);
                    return RedirectToAction("Index");
                }
                return View("Create", section);
            }
            catch
            {
                return View("Create", section);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var section = sectionRepository.Find(id);
                if (section != null)
                {
                    return View("Edit", section);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var section = sectionRepository.Find(id);
                if (section != null)
                {
                    if (TryUpdateModel(section))
                    {
                        try
                        {
                            sectionRepository.Update(id, section);
                            return RedirectToAction("Details", "Section", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", section);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var section = sectionRepository.Find(id);
                if (section != null)
                {
                    return View("Delete", section);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var section = sectionRepository.Find(id);
                if (section != null)
                    {
                        try
                            {
                                sectionRepository.Delete(id, section);
                                return RedirectToAction("Index", "Section");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", section);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
