﻿using System;
using System.Linq;
using System.Web.Mvc;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Net;


namespace AdminApplications.Controllers
{

    [Authorize]
    public class ResourceController : Controller
    {

        private ResourceRepository resourceRepository; 
                
        public ResourceController()
        {
            this.resourceRepository = new ResourceRepository();
        }

        public ResourceController(ResourceRepository resourceRepository)
        {
            this.resourceRepository = resourceRepository;
        }

        // GET: Member
        public ActionResult Index()
        {
            var resources = resourceRepository.Fill().OrderBy(rank => rank.name);

             return View("Index",resources);
        }

        // GET: Department/Details/5
        public ActionResult Details(int id)
        {
            var resource = resourceRepository.Find(id);
            return View("Details",resource);
        }

        // GET: Department/Create
        public ActionResult Create()
        {
            return View("Create", new Resource() { created_at = DateTime.Now, updated_at = DateTime.Now });
        }

        // POST: Department/Create
        [HttpPost]
        public ActionResult Create(Resource resource)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    resource.id = resourceRepository.Create(resource);
                    return RedirectToAction("Index");
                }
                return View("Create", resource);
            }
            catch
            {
                return View("Create", resource);
            }
        }

        public ActionResult Edit(int? id)
        {
            if (id!= null)
            {
                var resource = resourceRepository.Find(id);
                if (resource != null)
                {
                    return View("Edit", resource);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Member/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost( int? id)
        {
            if (id != null)
            {
                var resource = resourceRepository.Find(id);
                if (resource != null)
                {
                    if (TryUpdateModel(resource))
                    {
                        try
                        {
                            resourceRepository.Update(id, resource);
                            return RedirectToAction("Details", "Resource", new { id = id });
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    return View("Edit", resource);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // GET: Department/Delete/5
        public ActionResult Delete(int id)
        {
            if (id != null)
            {
                var resource = resourceRepository.Find(id);
                if (resource != null)
                {
                    return View("Delete", resource);
                }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        // POST: Department/Delete/5
        [HttpPost]
        public ActionResult Delete(int? id)
        {
            if (id != null)
            {
                var resource = resourceRepository.Find(id);
                if (resource != null)
                    {
                        try
                            {
                                resourceRepository.Delete(id, resource);
                                return RedirectToAction("Index", "Resource");
                            }
                        catch (Exception ex)
                            {
                            }
                        return View("Delete", resource);
                    }
                return HttpNotFound();
            }
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

    }
}
