﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using AdminApplications.Helpers;
using AdminApplicationsLibrary;
using System.Data.SqlClient;
using Dapper;
using System.Data.Entity;
using DapperExtensions;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace AdminApplications.DAL
{

    public class CTRCMemberRepository : MemberInfoRepository<CTRC_Member>
    {    }
    
    public class ContactRepository : MemberInfoRepository<Contact>
    { }

     public class UniversityAppointmentRepository : MemberInfoRepository<University_Appointment>
    {    }

    public class MemberListViewModelRepository : MemberInfoRepository<MemberListViewModel>
    {

        public IEnumerable<MemberListViewModel> GetAllLeadership()
        {
            IEnumerable<MemberListViewModel> result = null;
            var members = base.Fill();
            var leaders = from data in members.ToList()
                          where data.is_program_leader || data.is_facility_director || data.is_cac_member
                          select data;
            result = leaders.ToList();
            return result;
        }

        public IEnumerable<MemberListViewModel> GetAllResearch()
        {
            IEnumerable<MemberListViewModel> result = null;
            var members = base.Fill();
            var leaders = from data in members.ToList()
                          where data.is_clinical
                          select data;
            result = leaders.ToList();
            return result;
        }

    }
    public class MemberRepository : MemberInfoRepository<Member>
    {    }
    public class MembershipRepository : MemberInfoRepository<Membership>
    {    }
    public class CEC_MembershipRepository : MemberInfoRepository<CEC_Membership>
    {    }
    public class CRAC_MembershipRepository : MemberInfoRepository<CRAC_Membership>
    {    }

    public class CTRC_MemberRepository : MemberInfoRepository<CTRC_Member>
    {    }

    public class Sam_MemberRepository : MemberInfoRepository<Sam_Member>
    {    }

    public class Members_PublicationRepository : MemberInfoRepository<Members_Publication>
    {    }

    public class Member_CommunicationRepository : MemberInfoRepository<Member_Communication>
    {    }

    public class Contacts_MemberRepository : MemberInfoRepository<Contacts_Member>
    {    }

    public class Roles_UserRepository : MemberInfoRepository<Roles_User>
    {    }

    public class CTRC_MembershipRepository : Repository<CTRC_Membership>
    {    }

    public class Sam_MembershipRepository : Repository<Sam_Membership>
    {    }

    public class PublicationRepository : MemberInfoRepository<Publication>
    {    }

    public class CTRC_Membership_TypeRepository : LookupTable<CTRC_Membership_Type>
    {    }

    public class Member_Promotion_MediumRepository : LookupTable<Member_Promotion_Medium>
    { }

    public class Membership_StatusRepository : LookupTable<Membership_Status>
    { }

    public class DepartmentRepository : LookupTable<Department>
    {    }

    public class RankRepository : LookupTable<Rank>
    {    }

    public class ProgramRepository : LookupTable<Program>
    {    }

    public class SectionRepository : LookupTable<Section>
    {    }

    public class RoleRepository : LookupTable<Role>
    {    }

    public class ResourceRepository : LookupTable<Resource>
    {
    }
    
    public class UserRepository:LookupTable<User>
    {    }

    public class UCCCCExceptionRepository:LookupTable<UCCCCException>
    {    }

}