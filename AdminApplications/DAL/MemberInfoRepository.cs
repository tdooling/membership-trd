﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using AdminApplications.Helpers;
using System.Data.Entity;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace AdminApplications.DAL
{
    public abstract class MemberInfoRepository<T> : Repository<T>, IMemberIDGetRepository<T> where T : class
    {


        public virtual IEnumerable<T> FillByMemberId(int? memberId)
        {
            IEnumerable<T> result = null;
            if (memberId != null)
                {
                    try
                    {
                        using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                        {

                            String _endPoint = "api/" + typeof(T).Name + "/List/" + memberId.ToString();

                            HttpResponseMessage response = client.GetAsync(_endPoint).Result;
                            response.EnsureSuccessStatusCode();
                            if (response.IsSuccessStatusCode)
                            {
                                result = JsonConvert.DeserializeObject<IEnumerable<T>>(response.Content.ReadAsStringAsync().Result);
                            }
                        }
                    }
                    catch (Exception ex)
                    {

                        LogError(ex);
                    }
                }
            return result;
        }

    }
}
