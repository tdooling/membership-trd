﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AdminApplications.DAL
{

    public interface IMemberIDGetRepository<T> where T : class
    {
        IEnumerable<T> FillByMemberId(int? memberId);
    }

}
