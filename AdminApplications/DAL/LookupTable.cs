﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using AdminApplications.Helpers;
using System.Data.Entity;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Configuration;

namespace AdminApplications.DAL
{
    public abstract class LookupTable<T>:WSConnection,ILookupTable<T> where T:class
    {

        public virtual IEnumerable<T> Fill()
        {
            IEnumerable<T> result = null;
            try
            {
                using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                {

                    String _endPoint = "api/" + typeof(T).Name + "/Get";

                    HttpResponseMessage response = client.GetAsync(_endPoint).Result;
                    response.EnsureSuccessStatusCode();
                    if (response.IsSuccessStatusCode)
                    {
                        result = JsonConvert.DeserializeObject<IEnumerable<T>>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex);
                
            }
            return result;
        }

        public virtual T Find(int? id)
        {
            T result = null;
            if (id != null)
            {
                try
                {
                    using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                    {

                        String _endPoint = "api/" + typeof(T).Name + "/Get/" + id.ToString();

                        HttpResponseMessage response = client.GetAsync(_endPoint).Result;
                        response.EnsureSuccessStatusCode();
                        if (response.IsSuccessStatusCode)
                        {
                            result = JsonConvert.DeserializeObject<T>(response.Content.ReadAsStringAsync().Result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }
            }
            return result;
        }

        public virtual bool Update(int? id, T obj)
        {
            bool result = false;
            if (id != null)
            {
                if (obj != null)
                {
                    try
                    {
                        using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                        {

                            String _endPoint = "api/" + typeof(T).Name + "/Put/" + id.ToString();

                            HttpResponseMessage response = client.PutAsJsonAsync<T>(_endPoint, obj).Result;
                            response.EnsureSuccessStatusCode();
                            result = response.IsSuccessStatusCode;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                    }
                }
            }
            return result;
        }

        public virtual int Create(T obj)
        {
            int result = -1;
            if (obj != null)
            {
                try
                {
                    using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                    {

                        String _endPoint = "api/" + typeof(T).Name + "/Post";

                        HttpResponseMessage response = client.PostAsJsonAsync<T>(_endPoint, obj).Result;
                        response.EnsureSuccessStatusCode();
                        if (response.IsSuccessStatusCode)
                        {
                            string str = response.Content.ReadAsStringAsync().Result;
                            int.TryParse(str, out result);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex);
                }
            }
            return result;
        }

        public virtual bool Delete(int? id, T obj)
        {
            bool result = false;
            if (id != null)
            {
                if (obj != null)
                {
                    try
                    {
                        using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                        {

                            String _endPoint = "api/" + typeof(T).Name + "/Delete/" + id.ToString();

                            HttpResponseMessage response = client.DeleteAsync(_endPoint).Result;
                            response.EnsureSuccessStatusCode();
                            result = response.IsSuccessStatusCode;
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex);
                    }
                }
            }
            return result;
        }

    }
}