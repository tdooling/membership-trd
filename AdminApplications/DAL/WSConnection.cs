﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using AdminApplications.Helpers;
using System.Data.Entity;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Configuration;
using System.IO;
using AdminApplicationsLibrary;


namespace AdminApplications.DAL
{
    public abstract class WSConnection
    {

        static String _baseAddress = ""; 

        public string baseAddress
            {
                get
                    {
                        return _baseAddress;
                    }
                set
                    {
                        _baseAddress = value;
                    }
            }

        public WSConnection()
        {
            if (String.IsNullOrEmpty(baseAddress))
            {
                baseAddress = ConfigurationManager.AppSettings["PublicationsWS"];
            }
        }

        public WSConnection(String baseAddress)
        {
            this.baseAddress = baseAddress;
        }

        static public void SetBaseAddress(String baseAddress)
        {
            _baseAddress = baseAddress;
        }

        public HttpClient CreateClient(string baseAddress, MediaTypeWithQualityHeaderValue header)
        {
            var client = new HttpClient();
            if (!string.IsNullOrEmpty(baseAddress))
                client.BaseAddress = new Uri(baseAddress);
            else
                client.BaseAddress = new Uri(this.baseAddress);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(header);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("scheme","parameter");
            return client;
        }
        public static void LogError(Exception ex)
        {
            var repository = new UCCCCExceptionRepository();


            if (repository!=null)
            {
                var exception = new UCCCCException(ex);
                repository.Create(exception);
                repository = null;
            }
        }

    }
}