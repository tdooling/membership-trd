﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using DapperExtensions;
using AdminApplications.Helpers;
using System.Data.Entity;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;

namespace AdminApplications.DAL
{
    public abstract class Repository<T>:LookupTable<T>,IRepository<T> where T:class
    {

        public virtual IEnumerable<T> Fill(IEnumerable<int> ids)
        {
            IEnumerable<T> result = null;
            if (ids != null && ids.Count() > 0)
            {
                try
                {
                    using (var client = CreateClient("", new MediaTypeWithQualityHeaderValue("application/json")))
                    {

                        String _endPoint = "api/" + typeof(T).Name + "/ListArray";

                        HttpResponseMessage response = client.PostAsJsonAsync<IEnumerable<int>>(_endPoint, ids).Result;
                        response.EnsureSuccessStatusCode();
                        if (response.IsSuccessStatusCode)
                        {
                            result = JsonConvert.DeserializeObject<IEnumerable<T>>(response.Content.ReadAsStringAsync().Result);
                        }

                    }

                }
                catch (Exception ex)
                {
                    
                    LogError(ex);
                }
            }
            return result;
        }

    }
}
