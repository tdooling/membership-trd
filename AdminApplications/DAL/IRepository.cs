﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace AdminApplications.DAL
{

    interface IRepository<T> where T:class
        {

            IEnumerable<T> Fill(IEnumerable<int> ids);

        }

}
