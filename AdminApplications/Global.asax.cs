﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using DapperExtensions.Mapper;
using AdminApplications.DAL;

namespace AdminApplications
{

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            DapperExtensions.DapperExtensions.DefaultMapper = typeof(PluralizedAutoClassMapper<>); AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(AdminApplications.WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles); 
        }

    }

    public class CustomPluralizedMapper<T> : PluralizedAutoClassMapper<T> where T : class
    {
        protected  void Table(string tableName)
        {
            string table = tableName.ToLower();
            switch (table)
            {
                case "department":
                    TableName = "Departments";
                    break;
                case "ctrc_membership":
                    TableName = "ctrc_memberships";
                    break;
                case "ctrc_membership_type":
                    TableName = "ctrc_membership_types";
                    break;
                case "ctrc_member":
                    TableName = "ctrc_members";
                    break;
                case "crac_membership":
                    TableName = "crac_memberships";
                    break;
                case "cec_membership":
                    TableName = "cec_memberships";
                    break;
                case "rank":
                    TableName = "ranks";
                    break;
                case "section":
                    TableName = "sections";
                    break;
                case "sam_membership":
                    TableName = "sam_memberships";
                    break;
                case "sam_member":
                    TableName = "sam_members";
                    break;
                case "memberlistviewmodels":
                    TableName = "view_member_list";
                    break;
                case "memberlistviewmodel":
                    TableName = "view_member_list";
                    break;
                case "view_member_lists":
                    TableName = "view_member_list";
                    break;
                case "UniversityAppointment":
                    TableName = "university_appointments";
                    break;
                case "universityappointment":
                    TableName = "university_appointments";
                    break;
                case "universityappointments":
                    TableName = "university_appointments";
                    break;
                case "university_appointment":
                    TableName = "university_appointments";
                    break;
                case "member":
                    TableName = "members";
                    break;
                case "membership":
                    TableName = "memberships";
                    break;
                case "contact":
                    TableName = "contacts";
                    break;
                case "publication":
                    TableName = "publications";
                    break;
                case "members_publication":
                    TableName = "members_publications";
                    break;
                case "membership_status":
                    TableName = "membership_statuses";
                    break;
                default:
                    TableName = tableName;
                    break;
            }
            if (tableName.Equals("MemberListViewModel", StringComparison.CurrentCultureIgnoreCase))
            {
                TableName = "view_member_list";
            }
            //else if (tableName.Equals("ctrc_membership", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "ctrc_memberships";
            //}
            //else if (tableName.Equals("ctrc_membership_type", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "ctrc_membership_types";
            //}
            //else if (tableName.Equals("ctrc_member", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "ctrc_members";
            //}
            //else if (tableName.Equals("crac_membership", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "crac_memberships";
            //}
            //else if (tableName.Equals("cec_membership", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "cec_memberships";
            //}
            //else if (tableName.Equals("rank", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "ranks";
            //}
            //else if (tableName.Equals("section", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "sections";
            //}
            //else if (tableName.Equals("sam_membership", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "sam_memberships";
            //}
            //else if (tableName.Equals("sam_member", StringComparison.CurrentCultureIgnoreCase))
            //{
            //    TableName = "sam_members";
            //}

            base.Table(tableName);
        }
    }

}
