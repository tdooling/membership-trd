﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdminApplicationsLibrary;
using AdminApplications.DAL;
using System.Web;

namespace AdminApplications.Models
{
        public class MemberListEditViewModel : MemberListViewModel
        {

            public Member member;
            public IEnumerable<Membership> memberships;
            public IEnumerable<University_Appointment> universityAppointments;
            public IEnumerable<Contact> contacts;
            public IEnumerable<Publication> publications;

            public MemberListEditViewModel()
            {
                this.member = null;
                this.universityAppointments = null;
                this.contacts = null;
                this.memberships = null;
                this.publications = null;
            }

            public MemberListEditViewModel(Member member,
                IEnumerable<Membership> memberships,
                IEnumerable<University_Appointment> universityAppointments,
                IEnumerable<Contact> contacts,
                IEnumerable<Publication> publications)
            {
                this.member = member;
                this.memberships = memberships;
                this.universityAppointments = universityAppointments;
                this.contacts = contacts;
                this.publications = publications;
            }

            public MemberListEditViewModel(int? memberId)
                {
                    if (memberId != null)
                        {
                            member = (new MemberRepository()).Find(memberId);
                            if (member != null)
                                {
                                    memberships = (new MembershipRepository()).FillByMemberId(memberId);
                                    universityAppointments = (new UniversityAppointmentRepository()).FillByMemberId(memberId);
                                    publications = (new PublicationRepository()).FillByMemberId(member.id);
                                    var contactIds = from data in (new Contacts_MemberRepository()).FillByMemberId(memberId).ToList()
                                                     select data.contact_id;
                                    contacts = (new ContactRepository()).Fill(contactIds);
                                    
                                    var ranks = (new AdminApplications.DAL.RankRepository()).Fill();
                                    var programs = (new AdminApplications.DAL.ProgramRepository()).Fill();
                                    var departments = (new AdminApplications.DAL.DepartmentRepository()).Fill();

                                    if (memberships != null)
                                        {
                                            foreach (Membership item in memberships)
                                                {
                                                    item.Program = programs.FirstOrDefault(prog => prog.id == item.program_id).name;
                                                }
                                        }

                                    if (universityAppointments != null)
                                        {
                                            foreach (University_Appointment item in universityAppointments)
                                                {
                                                    item.Rank = ranks.FirstOrDefault(r => r.id == item.rank_id).name;
                                                    item.Department = departments.FirstOrDefault(dept => dept.id == item.department_id).name;
                                                }
                                        }
                               }
                        }
                }



    }
}